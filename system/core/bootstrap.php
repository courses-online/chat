<?php 
	/*
	|---------------------------------------------------------------|
	|                  WOW CHAT -Haz WOW CON EL CHAT                |
	|---------------------------------------------------------------|
	|                                                               |
	|              WOW CHAT HA SIDO REALIZADO POR Luis Garcia       |
	|                                                               |
	|---------------------------------------------------------------|
	 */

	/*
	|---------------------------------------------------------------|
	|  Definiendo constantes generales                              |
	|---------------------------------------------------------------|
	*/

	define('DS', DIRECTORY_SEPARATOR);
	define('ROOT','C:\xampp\htdocs\WOW'.DS );
	define('SYSTEM', ROOT.'system'.DS);

	define('LIBS', SYSTEM.'libs'.DS);
	define('CORE', SYSTEM.'core'.DS);
	define('VIEWS', SYSTEM.'views'.DS);

	


	/*
	|---------------------------------------------------------------|
	|  Iniciando Configuración		                                |
	|---------------------------------------------------------------|
	*/
 
	require_once(CORE.'config.php');
 


	/*
	|---------------------------------------------------------------|
	|  Autocargando nuestras librerias                              |
	|---------------------------------------------------------------|
	*/
	#Extensiones a cargar
	
	spl_autoload_extensions('.php,.php3,.php4');

	#funcion __autoload
	
	spl_autoload_register('__autoload');

	function __autoload($c)
	{
		#incluyendo la carpeta libs
		set_include_path(LIBS);

		#Cargando las clases.
		spl_autoload($c);
	}

        /*
        |---------------------------------------------------------------|
        |  Configuración de la página web.                              |
        |---------------------------------------------------------------|
        */

        ini_set('default_charset', $config->charset);
     
        




 ?>