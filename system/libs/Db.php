<?php 
	class Db extends PDO{
		public function __contruct($dbhost,$dbname,$dbuser,$dbpass){

			try{
				parent::__contruct('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpass);

				# Mostrando Errores y Excepciones

				parent::setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

			}catch(Exception $e){
				trigger_error($e->getMessage(),E_USER_ERROR)
			}

		}
	}


?>