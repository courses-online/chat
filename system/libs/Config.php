<?php

	/*
	|---------------------------------------------------------------|
	|  Funcionalidades de Configuración                             |
	|---------------------------------------------------------------|
	*/ 
	Class Config{

		private static $params;
		private static $instance;

		public function __construct()
		{
			self::$params = [];
		}

		public static function get($k){
			#¿Existe $k dentro de mis párametros?
			if (isset(self::$params[$k])) 

			return self::$params[$k];
			#¿No Existe?
			else

			return false;
		}
		public static function set($k,$v){
			#¿No Existe?
			if (!isset(self::$params[$k])) 

			return self::$params[$k]=$v;

			#¿Existe?
			else

			return false;
		}

		public static function main(){
			#¿Ha sido  definida?
			if (!isset(self::$instance)) {

				$c = __CLASS__;

				self::$instance = new $c();
			}

			return self::$instance;
		}
	}
 ?>
