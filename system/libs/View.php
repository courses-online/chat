<?php

        /*
        |---------------------------------------------------------------|
        |  Funcionalidades de Configuración                             |
        |---------------------------------------------------------------|
        */ 
        Class View {

                private static $params;
                private static $instance;

                public function __construct()
                {
                        self::$params = [];
                }

                public static function get($k){
                        #¿Existe $k dentro de mis párametros?
                        if (isset(self::$params[$k])) 

                        return self::$params[$k];
                        #¿No Existe?
                        else

                        return false;
                }
                public static function set($k,$v){
                        #¿No Existe?
                        if (!isset(self::$params[$k])) 

                        return self::$params[$k]=$v;

                        #¿Existe?
                        else

                        return false;
                }
                public static function load($v){
                        #Dirección del Archivo
                        $f=VIEWS.$v.'.html';

                        #¿El Archivo Existe?
                        if (!file_exists($f))   trigger_error('El archivo '.$v.' no existe. ', E_USER_ERROR);
                        
                        # ¿El archivo existe y es legible?
                        if (!is_readable($f))  trigger_error('El Archivo '.$v.' no es legible. ',E_USER_ERROR);

                        #Renderizando la vista
                        self::render($f);
                }

                public static function render($f){

                        #Obteiendo ell contenido de nueestra vista.
                        $view= file_get_contents($f);

                        foreach (self::$params as $k => $v) {
                                
                                $view=str_replace('{{'.$k.'}}', $v, $view);
                        }
                        #Dibujando Nuestra Vista
                                return print $view;
                        



                }

                public static function main(){
                        #¿Ha sido  definida?
                        if (!isset(self::$instance)) {

                                $c = __CLASS__;

                                self::$instance = new $c();
                        }

                        return self::$instance;
                }
        }
 ?>
