<?php 
	/*
	|---------------------------------------------------------------|
	|                  WOW CHAT -Haz WOW CON EL CHAT                |
	|---------------------------------------------------------------|
	|                                                               |
	|              WOW CHAT HA SIDO REALIZADO POR Luis Garcia       |
	|                                                               |
	|---------------------------------------------------------------|
	 */

	/*
	|---------------------------------------------------------------|
	|  Comprobando PDO                                              |
	|---------------------------------------------------------------|
	*/

	if (!extension_loaded('PDO')) 
		
	trigger_error('La Extencion no ha sido ejecutada', E_USER_ERROR);
	
	/*
	|---------------------------------------------------------------|
	|  Reportando Errores                                           |
	|---------------------------------------------------------------|
	*/
	error_reporting(E_ALL);

	/*
	|---------------------------------------------------------------|
	|  Mostrando los Errores                                           |
	|---------------------------------------------------------------|
	*/
	ini_set('display_errors', 1);

	/*
	|---------------------------------------------------------------|
	|  Comprobando Version de PHP                                   |
	|---------------------------------------------------------------|
	*/
        
	if (version_compare(PHP_VERSION, '5.6.15','<')) 

	trigger_error("Tu versión de PHP, no es compatible con el script.",E_USER_ERROR);
	
	/*
	|---------------------------------------------------------------|
	|  Iniciando Bootstrap                                          |
	|---------------------------------------------------------------|
	*/
	require_once('C:\xampp\htdocs\WOW\system\core\bootstrap.php');

             /*
        |---------------------------------------------------------------|
        |  Vista de la página web.                                      |
        |---------------------------------------------------------------|
        */
        $view= View::main();
        # Configuraciones de la vista
        $view->set('siteurl',$config->siteurl);
        $view->set('sitename',$config->sitename);
        # Cargando vista
        $view->load("index");


 ?>